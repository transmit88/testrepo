﻿using NatureReserveSimulation.Animals;

namespace NatureReserveSimulation.Statistics
{
    public class Statistic
    {

        public void Details(string text)
        {
            Console.WriteLine(text);
        }

        public void Summary(int count, List<Animal> animals)
        {
            Console.WriteLine($"Turn {count}");
            Console.WriteLine($"Alive: {animals.Where(animal => animal.IsActive()).Count()}");
            Console.WriteLine($"Dead: {animals.Where(animal => !animal.IsActive()).Count()}");
            Console.WriteLine();
        }

        public void Finally(int minLifespan, int maxLifespan, double avgLifespan)
        {
            Console.WriteLine($"Minimum lifespan: {minLifespan}");
            Console.WriteLine($"Maximum lifespan: {maxLifespan}");
            Console.WriteLine($"Average lifespan: {Math.Round(avgLifespan)}");
        }
    }

}
