﻿using NatureReserveSimulation.Animals;
using NatureReserveSimulation.Foods;

namespace NatureReserveSimulation
{
    public static class Data
    {

        public static List<Animal> Animals = new List<Animal>
        {
            new Bear(),
            new Gorilla(),
            new Lizard(),
            new Sheep(),
            new Snake(),
        };

        public static List<Food> Foods = new List<Food>(Animals)
        {
            new Egg(),
            new Fish(),
            new Fruits(),
            new Grass(),
            new Vegetable(),
        };
    }
}
