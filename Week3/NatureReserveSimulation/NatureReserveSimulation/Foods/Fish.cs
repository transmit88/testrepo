﻿namespace NatureReserveSimulation.Foods
{
    public class Fish : Food
    {
        private const int defaultNutrioneValue = 4;

        public Fish()
            : base(defaultNutrioneValue)
        {
        }

    }
}
