const btn = document.getElementById("input_form");

const firstName = document.getElementById("first_name");
const lastName = document.getElementById("last_name");
const age = document.getElementById("age");
const rating = document.getElementById("rating");
const description = document.getElementById("description");
const feedback = document.getElementById("feedback");


btn.addEventListener("click", function(e){
    e.preventDefault()

    const createForm = document.createElement("div");
    createForm.classList.add("box");

    createForm.innerHTML = `
    <h2>Name: ${firstName.value} ${lastName.value}</h2>
    <p>Age: ${age.value}</p>
    <p>Rating: ${rating.value}</p>
    <p>Description: ${description.value}</p>
    <button class="delete">Delete</button>
  `;

    feedback.appendChild(createForm)

    firstName.value = '';
    lastName.value = '';
    age.value = '';
    description.value = '';
});


feedback.addEventListener("click", function(e){
    if (e.target.matches(".delete")) {
      e.target.closest(".box").remove();
    }
  });
