CREATE DATABASE Subscribe;

USE MOVIES;

USE Subscribe;


CREATE TABLE [User](
	id INT NOT NULL IDENTITY PRIMARY KEY,
	email VARCHAR(255), CHECK (email LIKE '_%@__%.__%'),
	password VARCHAR(20),
	name VARCHAR(40),
	failed_attempts INT, CHECK (failed_attempts <= 3),
	age INT, CHECK(age > 0),
	gender CHAR(1), CHECK(gender IN('M', 'F')),
	countryId INT NOT NULL,
);

CREATE TABLE DebitCard(
	id INT NOT NULL PRIMARY KEY,
	userId INT FOREIGN KEY REFERENCES [User](id) NOT NULL,
	number VARCHAR(16) NOT NULL,
	exp_date DATE NOT NULL,
);

CREATE TABLE Country(
	id INT NOT NULL IDENTITY PRIMARY KEY,
	name VARCHAR(40)
);

CREATE TABLE Product(
	id INT NOT NULL IDENTITY PRIMARY KEY,
	name VARCHAR(40) NOT NULL,
	typeId INT NOT NULL,
	minAge INT NOT NULL,
	originCountry INT NOT NULL,
);

CREATE TABLE UserLikes(
	productId INT FOREIGN KEY REFERENCES Product(id),
	userId INT FOREIGN KEY REFERENCES [User](id),
	PRIMARY KEY(productId, userId),
);

CREATE TABLE DistCountry(
	productId INT FOREIGN KEY REFERENCES Product(id),
	coutryId INT FOREIGN KEY REFERENCES Country(id),
	PRIMARY KEY(productId, coutryId),	
);

CREATE TABLE Comment(
	id INT IDENTITY PRIMARY KEY,
	userId INT FOREIGN KEY REFERENCES [User](id),
	productId INT FOREIGN KEY REFERENCES Product(id),
	text VARCHAR(255)
);

CREATE TABLE Genre(
	id INT IDENTITY PRIMARY KEY,
	name VARCHAR(40) NOT NULL,
);

CREATE TABLE Type(
	id INT IDENTITY PRIMARY KEY,
	name VARCHAR(40) NOT NULL UNIQUE,
	genreId INT REFERENCES Genre(id),
);

CREATE TABLE Subs(
	id INT IDENTITY PRIMARY KEY,
	userId INT NOT NULL,
	duration INT NOT NULL,
	productSubPriceId INT NOT NULL,
);

CREATE TABLE ProductSubPrice(
	id INT IDENTITY PRIMARY KEY,
	productId INT FOREIGN KEY REFERENCES Product(id),
	subTypeId INT NOT NULL,
	price DECIMAL(6,2),
);

CREATE TABLE SubType(
	id INT IDENTITY PRIMARY KEY,
	name VARCHAR(40),
);

--


ALTER TABLE Product
ADD CONSTRAINT FK_Product_Type
FOREIGN KEY (typeId) REFERENCES Type(id);

ALTER TABLE Subs
ADD CONSTRAINT FK_Subs_ProductSubPrice
FOREIGN KEY (productSubPriceId) REFERENCES ProductSubPrice(id);

ALTER TABLE ProductSubPrice
ADD CONSTRAINT FK_ProductSubPrice_SubType
FOREIGN KEY (id) REFERENCES SubType(id);



