use ships;

CREATE INDEX idx_country ON classes (country);

CREATE INDEX idx_class ON ships (class);

CREATE INDEX idx_ship_result ON outcomes (ship, result);

CREATE INDEX idx_battle ON outcomes (battle);


DROP INDEX idx_country ON classes;

DROP INDEX idx_class ON ships;

DROP INDEX idx_ship_result ON outcomes;

DROP INDEX idx_battle ON outcomes;

