CREATE DATABASE Task1

USE Task1

CREATE TABLE Product(
Model CHAR(4),
Maker CHAR(1),
[Type] CHAR(7),
);

CREATE TABLE Printer(
Code INT,
Model CHAR(4),
Price DECIMAL(6,2),
);

INSERT INTO Product (Model, Maker, Type)
VALUES ('530i', 'B', 'CarType');

INSERT INTO Printer (Code, Model, Price)
VALUES (10, 'TEST', 1000.50);

ALTER TABLE Printer
ADD [Type]  CHAR(6)
CHECK([Type] IN ('laser', 'matrix', 'jet'));

ALTER TABLE Printer
ADD Color CHAR(1)
CHECK (Color IN ('y', 'n')); 

ALTER TABLE Printer
DROP COLUMN Price;

DROP TABLE Printer;
DROP TABLE Product;
