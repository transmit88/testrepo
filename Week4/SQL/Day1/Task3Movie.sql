/****** Script for SelectTopNRows command from SSMS  ******/
SELECT TOP (1000) [CERT#]
      ,[NAME]
      ,[ADDRESS]
      ,[NETWORTH]
  FROM [movies].[dbo].[MOVIEEXEC]

INSERT INTO moviestar([name], gender, birthdate) VALUES('Nicole Kidman', 'F', '1967-06-20')

DELETE FROM movieexec WHERE NETWORTH < 30000000

INSERT INTO MOVIESTAR ([NAME], [ADDRESS], GENDER, BIRTHDATE)
VALUES('Ivan Ivanov', NULL, 'M', '1967-06-20');

DELETE FROM MOVIESTAR WHERE ADDRESS IS NULL;
