CREATE DATABASE Flights;

USE Flights;

CREATE TABLE Airline(
Code CHAR(2) PRIMARY KEY,
[Name] VARCHAR(20) UNIQUE,
Country VARCHAR(20) NOT NULL);

DROP TABLE Airline;

INSERT INTO Airline
VALUES
  ('AZ', 'Alitalia', 'Italy'),
  ('BA', 'British Airways', 'United Kingdom'),
  ('LH', 'Lufthansa', 'Germany'),
  ('SR', 'Swissair', 'Switzerland'),
  ('FB', 'Bulgaria Air', 'Bulgaria'),
  ('AF', 'Air France', 'France'),
  ('TK', 'Turkish Airlines', 'Turkey'),
  ('AA', 'American Airlines', 'United States');

CREATE TABLE Flight (
Fnumber VARCHAR(6) PRIMARY KEY,
Airline_Operator CHAR(2) NOT NULL,
Dep_Airport CHAR(3) NOT NULL,
Arr_Airport CHAR(3) NOT NULL,
Flight_Time TIME,
Flight_Durtion SMALLINT,
Airline SMALLINT) ;

DROP TABLE Flight

INSERT INTO FLIGHT
VALUES
  ('FB1363', 'AZ', 'SOF', 'ORY', '12:45', 100, '738'),
  ('FB1364', 'AZ', 'CDG', 'SOF', '10:00', 120, '321'),
  ('SU2060', 'AZ', 'LBG', 'SOF', '11:10', 110, '738'),
  ('SU2061', 'TK', 'SOF', 'JFK', '13:00', 110, '320'),
  ('FB363', 'FB', 'SOF', 'ORD', '15:10', 110, '738'),
  ('FB364', 'FB', 'LHR', 'SOF', '21:05', 120, '738');


CREATE TABLE Airplane(
Code CHAR(3) PRIMARY KEY,
[Type] VARCHAR(15) NOT NULL,
Seats SMALLINT,
[Year] SMALLINT,
);

DROP TABLE Airplane;

INSERT INTO Airplane
VALUES
  ('319', 'Airbus A319', 150, 1993),
  ('320', 'Airbus A320', 280, 1984),
  ('321', 'Airbus A321', 150, 1989),
  ('100', 'Fokker 100', 80, 1991),
  ('738', 'Boeing 737-800', 90, 1997),
  ('735', 'Boeing 737-800', 90, 1995);


CREATE TABLE Airport(
Code CHAR(3) PRIMARY KEY,
[Name] VARCHAR(31) UNIQUE,
Country VARCHAR(20) NOT NULL,
City VARCHAR(15) NOT NULL
);

DROP TABLE Airport;

INSERT INTO Airport
VALUES
  ('SOF', 'Sofia International', 'Bulgaria', 'Sofia'),
  ('CDG', 'Charles De Gaulle', 'France', 'Paris'),
  ('ORY', 'Orly', 'France', 'Paris'),
  ('LBG', 'Le Bourget', 'France', 'Paris'),
  ('JFK', 'John F Kennedy International', 'United States', 'New York'),
  ('ORD', 'Chicago O''Hare International','United States', 'Chicago'),
  ('FCO', 'Leonardo da Vinci International','Italy', 'Rome'),
  ('LHR', 'London Heathrow', 'United Kingdom', 'London');


CREATE TABLE Customer(
Id INT PRIMARY KEY,
Fname VARCHAR(20) NOT NULL,
Lname VARCHAR(20) NOT NULL,
Email VARCHAR(50) CHECK(Email LIKE '%_@__%.__%' AND LEN (Email) > 6),
);


DROP TABLE Customer;

INSERT INTO Customer
VALUES
  (1, 'Petar', 'Milenov', 'petter@mail.com'),
  (2, 'Dimitar', 'Petrov', 'petrov@mail.com'),
  (3, 'Ivan', 'Ivanov', 'ivanov@mail.com'),
  (4, 'Petar', 'Slavov', 'slavov@mail.com'),
  (5, 'Bogdan', 'Bobov', 'bobov@mail.com');


CREATE TABLE Agency(
[Name] VARCHAR(15) NOT NULL,
Country VARCHAR(15) NOT NULL,
City VARCHAR(15) NOT NULL,
Phone VARCHAR(10),
);

ALTER TABLE Agency
ADD CONSTRAINT pk PRIMARY KEY ([Name],Country);

DROP TABLE Agency

INSERT INTO Agency
VALUES
  ('Travel One', 'Bulgaria', 'Sofia', '0783482233'),
  ('Travel Two', 'Bulgaria', 'Plovdiv', '02882234'),
  ('Travel Tour', 'Bulgaria', 'Sofia', NULL),
  ('Aerotravel', 'Bulgaria', 'Varna', '02884233');


CREATE TABLE Booking(
Code CHAR(6) PRIMARY KEY,
Agency CHAR(11) NOT NULL,
Airline_Code CHAR(2) NOT NULL,
Flight_Number CHAR(6) NOT NULL,
CustomerId SMALLINT,
Booking_Date DATE,
Flight_Date DATE,
Price DECIMAL,
[Status] SMALLINT);

ALTER TABLE Booking
WITH 
CHECK ADD  CONSTRAINT CK_DATES CHECK(Booking_Date <= Flight_Date);

DROP TABLE Booking

INSERT INTO Booking
VALUES
  ('YN298P', 'Travel One', 'FB', 'FB1363', 1, '2013-11-18', '2013-12-25', 300, 0),
  ('YA298P', 'Travel Two', 'FB', 'FB1364', 2, '2013-12-18', '2013-12-25', 300, 1),
  ('YB298P', 'Travel Tour', 'FB', 'SU2060', 3, '2014-01-18', '2014-02-25', 400, 0),
  ('YC298P', 'Travel One', 'FB', 'SU2061', 4, '2014-11-11', '2014-11-25', 350, 0),
  ('YD298P', 'Travel Tour', 'FB', 'FB363', 1, '2013-11-03', '2013-12-20', 250, 1),
  ('YE298P', 'Aerotravel', 'FB', 'FB364', 2, '2013-11-07', '2013-12-21', 150, 0);
