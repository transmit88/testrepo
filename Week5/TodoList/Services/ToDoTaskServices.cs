﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using TodoList.Contracts;
using TodoList.Models;

namespace TodoList.Services
{
    public class ToDoTaskServices : IToDoTaskServices
    {

        private readonly ToDoLIstContext _context;

        public ToDoTaskServices(ToDoLIstContext context)
        {
            _context = context;
        }

        [HttpGet]
        public List<TodoTask> AllTasks()
        {
            List<TodoTask> tasks = new List<TodoTask>();

            tasks = (from task in _context.TodoTasks
                     join assignment in _context.Assignments
                         on task.AssignmentId equals assignment.AssigmentId
                     select new TodoTask()
                     {
                         Id = task.Id,
                         Name = task.Name,
                         Date = task.Date,
                         Done = task.Done,
                         Assignment = assignment
                     }).ToList();


            return tasks;
        }

        public void CreateTask(TodoTask todoTask)
        {
            TodoTask taskToAdd = new TodoTask();

            taskToAdd.Name = todoTask.Name;
            taskToAdd.Date = todoTask.Date;
            taskToAdd.Done = false;
            taskToAdd.Assignment = new Assignment
            {
                AssignedTo = todoTask.Assignment.AssignedTo,
                CreatedBy = todoTask.Assignment.CreatedBy
            };

            _context.TodoTasks.Add(taskToAdd);
            _context.SaveChanges();

        }

        public TodoTask DetailsTask(int id)
        {
            TodoTask detailTask = new TodoTask();


            detailTask = (from task in _context.TodoTasks
                          join assignment in _context.Assignments
                              on task.AssignmentId equals assignment.AssigmentId
                          select new TodoTask
                          {
                              Id = task.Id,
                              Name = task.Name,
                              Date = task.Date,
                              Done = task.Done,
                              Assignment = assignment
                          }).FirstOrDefault(x => x.Id == id);


            return detailTask;
        }

        public TodoTask EditTask(TodoTask model)
        {
            TodoTask oldTask = _context.TodoTasks.FirstOrDefault(x => x.Id == model.Id);

            oldTask.Name = model.Name;
            oldTask.Date = model.Date;
            oldTask.Done = model.Done;
            oldTask.Assignment = model.Assignment;

            _context.SaveChanges();


            return model;
        }

        public TodoTask EditViewTask(int id)
        {
            TodoTask editTask = new TodoTask();


            editTask = (from task in _context.TodoTasks
                        join assignment in _context.Assignments
                            on task.AssignmentId equals assignment.AssigmentId
                        select new TodoTask
                        {
                            Id = task.Id,
                            Name = task.Name,
                            Date = task.Date,
                            Done = task.Done,
                            Assignment = assignment
                        }).FirstOrDefault(x => x.Id == id);


            return editTask;
        }

        public async Task DeleteTask(int id)
        {

            TodoTask removeTask = _context.TodoTasks.FirstOrDefault(x => x.Id == id);

            _context.TodoTasks.Remove(removeTask);

            _context.SaveChanges();

        }
    }
}
