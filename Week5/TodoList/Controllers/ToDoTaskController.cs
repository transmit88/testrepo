﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using TodoList.Contracts;
using TodoList.Models;
using TodoList.Services;

namespace TodoList.Controllers
{
    public class ToDoTaskController : Controller
    {
        private readonly IToDoTaskServices _IToDoTaskServices;

        public ToDoTaskController(IToDoTaskServices toDoTaskServices)
        {
            _IToDoTaskServices = toDoTaskServices;
        }

        [HttpGet]
        public IActionResult Index()
        {
            //List<TodoTask> tasks = new List<TodoTask>();

            var result = _IToDoTaskServices.AllTasks();
           
            //if (tasks.Count > 0)
            //{
            //    return View(tasks);
            //}

            return View(result);
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(TodoTask task)
        {
            _IToDoTaskServices.CreateTask(task);
            
            return RedirectToAction("Index");
        }

        public IActionResult Details(int id)
        {
            TodoTask detailTask = new TodoTask();

            detailTask = _IToDoTaskServices.DetailsTask(id);

            return View(detailTask);
        }

        public IActionResult Edit(int id)
        {
            TodoTask editTask = new TodoTask();

            editTask = _IToDoTaskServices.EditViewTask(id);

            return View(editTask);
        }

        [HttpPost]
        public IActionResult Edit(TodoTask model)
        {
            model =  _IToDoTaskServices.EditTask(model);

            return RedirectToAction("Index");
        }


        public IActionResult Delete(int id)
        { 
            _IToDoTaskServices.DeleteTask(id);

            return RedirectToAction("Index");
        }
    }
}
