﻿using TodoList.Models;

namespace TodoList.Contracts
{
    public interface IToDoTaskServices
    {
        public List<TodoTask> AllTasks();

        public void CreateTask(TodoTask todoTask);

        public TodoTask DetailsTask(int id);

        public TodoTask EditViewTask(int id);

        public TodoTask EditTask(TodoTask model);

        public Task DeleteTask(int id);
    }
}
