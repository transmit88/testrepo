﻿using System.Xml.Linq;

namespace Task2 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {
            var node76 = new BinaryTree(76, null, null);
            var node61 = new BinaryTree(61, null, null);
            var node35 = new BinaryTree(35, null, null);
            var node69 = new BinaryTree(69, node61, node76);
            var node38 = new BinaryTree(38, node35, null);
            var node24 = new BinaryTree(24, null, null);
            var node11 = new BinaryTree(11, null, node24);
            var node52 = new BinaryTree(52, node38, node69);
            var root = new BinaryTree(25, node11, node52);

           
            int heightTree = HeightTree(root);

            Console.WriteLine(heightTree);
        }

        public static int HeightTree(BinaryTree root)
        {
            int heightLeft = 0;
            int heightRight = 0;

            if (root == null || (root != null && root.Left == null && root.Right == null))
            {
                return 0;
            }
            else
            {
                heightLeft = HeightTree(root.Left);
                //Console.WriteLine(heightLeft);
                heightRight = HeightTree(root.Right);
                //Console.WriteLine(heightRight);

                if (heightLeft > heightRight)
                {
                    return heightLeft + 1;
                }
                else
                {
                    return heightRight + 1;
                }
            }
        }
    }
}