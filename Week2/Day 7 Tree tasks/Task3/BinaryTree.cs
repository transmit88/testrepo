﻿namespace Task3
{
    public class BinaryTree
    {

        public BinaryTree(int value, BinaryTree left, BinaryTree right)
        {
            Value = value;
            Left = left;
            Right = right;
        }

        public int Value { get; set; }

        public BinaryTree Left { get; set; }

        public BinaryTree Right { get; set; }
    }


}
