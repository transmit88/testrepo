﻿using System;
using System.Runtime.ConstrainedExecution;

namespace Task1 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, string> carNumberToOwner = new Dictionary<string, string>();

            carNumberToOwner.Add("CB5839TT", "Dimitrina Dimitrova");
            carNumberToOwner.Add("CB1234TT", "Ivan Ivanov");
            carNumberToOwner.Add("CB4154HH", "Petar Ptrov");
            carNumberToOwner.Add("CB3333BB", "Stoqn Stoqnov");
            carNumberToOwner.Add("CB5555BB", "Dimitar Dimitrov");
            carNumberToOwner.Add("CB4444BB", "Stefan Stefanov");
            carNumberToOwner.Add("CB4445BB", "Stefan Stefanov");
            carNumberToOwner.Add("CB5838TT", "Dimitrina Dimitrova");


            List<string> returnCarNumber = CarNumber("Dimitrina Dimitrova", carNumberToOwner);
            //List<string> ownerWithMoreFromOneCar = OwnerWithMoreFromOneCar(carNumberToOwner);

            foreach (var item in returnCarNumber)
            {
                Console.WriteLine($"{item}");
            }

            //foreach (var item in ownerWithMoreFromOneCar)
            //{
            //    Console.WriteLine(item);
            //}
        }

        public static List<string> CarNumber(string owner, Dictionary<string, string> carNumberToOwner)
        {

            List<string> result = new List<string>();

            foreach (var item in carNumberToOwner)
            {
                if (item.Value.Contains(owner))
                {
                    result.Add(item.Key);
                }
            }

            return result;
        }

        public static List<string> OwnerWithMoreFromOneCar(Dictionary<string, string> carNumberToOwner)
        {

            Dictionary<string, string> ownerWithOneCar = new Dictionary<string, string>();
            List<string> ownerWithMoreFromOneCar = new List<string>();

            foreach (var item in carNumberToOwner)
            {

                if (!ownerWithOneCar.ContainsValue(item.Value))
                {
                    ownerWithOneCar.Add(item.Key, item.Value);
                }
                else
                {
                    ownerWithMoreFromOneCar.Add(item.Value);
                }

            }

            return ownerWithMoreFromOneCar;
        }
    }
}