﻿
namespace Task2 // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {
            int[] arr1 = { 1, 2, 3, 4, 8 };
            int[] arr2 = { 2, 3, 4, 5, 6, 7, 8};


            List<int> repeatEl = FindRepeatEl(arr1, arr2);

            for (int i = 0; i < repeatEl.Count; i++)
            {
                Console.WriteLine($"{repeatEl[i]}");
            }
        }

        public static List<int> FindRepeatEl(int[] arr1, int[] arr2)
        {
            List<int> result = new List<int>();

            for (int i = 0; i < arr1.Length; i++)
            {
                for (int j = 0; j < arr2.Length; j++)
                {
                    if (arr1[i] == arr2[j])
                    {
                        result.Add(arr1[i]);
                    }
                }
            }

            if (result.Count == 0)
            {
                throw new ArgumentException("Collection is empty");
            }

            return result;
        }
    }
}