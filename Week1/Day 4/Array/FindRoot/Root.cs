﻿using System;

namespace FindRoot // Note: actual namespace depends on the project name.
{
    public class Root
    {
        static void Main(string[] args)
        {

            int input = int.Parse(Console.ReadLine());

            double root = FindRoot(input);

            Console.WriteLine(root);
        }

        public static int FindRoot(double input)
        {
            
            double cubeRoot = input / 3.0;
            double precision = 0.000001;
            double previous;

            do
            {
                previous = cubeRoot;
                cubeRoot = (2.0 * cubeRoot + input / (cubeRoot * cubeRoot)) / 3.0;
            }
            while (Math.Abs(previous - cubeRoot) > precision);

            if (input == 0) return 0;

               
            cubeRoot = (double)System.Math.Round(cubeRoot, 2);

            bool isInt = cubeRoot % 1 == 0;

            if (!isInt)
            {
                return 0;
            }

            return (int)cubeRoot;
            
        }
    }
}