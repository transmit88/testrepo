﻿

namespace LinkedList // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {


            LinkedList<int> linkedList = new LinkedList<int>();
            linkedList.AddLast(1);
            linkedList.AddLast(2);
            linkedList.AddLast(3);
            linkedList.AddLast(4);
            linkedList.AddLast(5);
            linkedList.AddLast(6);

            LinkedList<int> listWithoutRemoveElement = RemoveElement(linkedList);

            foreach (var item in listWithoutRemoveElement)
            {
                Console.WriteLine(item);
            }
        }

        public static LinkedList<int> RemoveElement(LinkedList<int> linkedList)
        {
            int length = (linkedList.Count - 1) / 2;
            int number = linkedList.ElementAt(length);

            linkedList.Remove(number);

            return linkedList;
        }
    }
}