﻿namespace LinkedList // Note: actual namespace depends on the project name.
{
    public class Program
    {
        //static void Main(string[] args)
        //{

        //    Stack<int> A = new Stack<int>();
        //    Stack<int> B = new Stack<int>();
        //    Stack<int> C = new Stack<int>();

        //    int disks = int.Parse(Console.ReadLine());

        //    int moving = MovingDisks(disks, A, B, C);

        //    Console.WriteLine("Hello");
        //}

        //public int MovingDisks(int moving, Stack<int> a, Stack<int> b, Stack<int> c)
        //{

        //    int movingDisk = moving * 2 + 1;

        //    for (int i = 0; i < movingDisk; i++)
        //    {
        //        c.Push(movingDisk - 1);
        //        Console.WriteLine(c);
        //        b.Push()
        //    }

        //}

        public static void Main(String[] args)
        {
            char startPeg = 'A'; // start tower in output
            char endPeg = 'C'; // end tower in output
            char tempPeg = 'B'; // temporary tower in output
            int totalDisks = 8; // number of disks

            solveTowers(totalDisks, startPeg, endPeg, tempPeg);
        }

        private static void solveTowers(int n, char startPeg, char endPeg, char tempPeg)
        {
            if (n > 0)
            {
                solveTowers(n - 1, startPeg, tempPeg, endPeg);
                Console.WriteLine("Move disk from " + startPeg + ' ' + endPeg);
                solveTowers(n - 1, tempPeg, endPeg, startPeg);

            }
        }
    }
}