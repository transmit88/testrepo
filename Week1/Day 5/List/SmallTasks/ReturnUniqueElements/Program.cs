﻿
namespace ReturnUniqueElements // Note: actual namespace depends on the project name.
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<int> input = new List<int>() {1, 2, 3, 4 ,3 ,5, 2, 3, 5};

            var uniqNumber = FindUniqNumber(input);

            foreach (int i in uniqNumber)
            {
                Console.WriteLine(i);
            }
        }

        public static List<int> FindUniqNumber(List<int> input)
        {

            var unique_items = new HashSet<int>(input);
            List<int> result = new List<int>(unique_items);

            return result;
        }
    }
}