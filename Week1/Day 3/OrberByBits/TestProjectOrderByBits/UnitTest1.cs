using OrberByBits;

namespace TestProjectOrderByBits
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {

            uint[] input = { 4, 68, 29, 12, 10, 30 };
            uint[] expected = { 4, 68, 12, 10, 29, 30 };

            uint[] result = Program.GetSortedArrayByBits(input);

            Assert.AreEqual(expected, result);


        }
    }

    // Not working ??

}