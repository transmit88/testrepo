﻿namespace OrberByBits
{
    public class Program
    {


        // Helper function to get the sum of bits of a numbers
        static uint CalcBits(uint num)
        {
            uint res = 0;

            while (num != 0)
            {
                res += num & 1;
                num >>= 1;
            }

            return res;
        }


        // Sort the bits in an array
        static uint[] SortBitsNumbers(uint[] array)
        {

            // Array with sums of bits from the numbers
            uint[] bits = new uint[array.Length];


            // Loop over the array and calc the bits of each number
            for (uint i = 0; i<array.Length; i++)
            {
                uint val = CalcBits(array[i]);
                bits[i] = val;

            }


            // Sort the array
            Array.Sort(bits);
            return bits;

        }

        // Transform back to a normal array [double loop to find the matching value]
        static uint[] TransformToNumsArray(uint[] input, uint[] bitsArr)
        {
            uint[] result = new uint[bitsArr.Length];


            // Valuess of the bitsArray
            for(int i= 0; i< bitsArr.Length; i++)
            {
               
                // Values of the input
                for(uint j= 0; j<input.Length; j++)
                {

                    // If the bits = to the number && we haven't pushed it yet -> add it to the results
                    if (CalcBits(input[j]) == bitsArr[i] && Array.IndexOf(result, input[j]) <= 0)
                    {
                        result[i] = input[j];
                        break;

                    }
                }
            }

            return result;
        }


        // Call the main function here :D
        public static uint[] GetSortedArrayByBits(uint[] input)
        {
            uint[] bitsArray = SortBitsNumbers(input);
            uint[] answer = TransformToNumsArray(input, bitsArray);

            return answer;

        }

        static void Main(string[] args)
        {
            Console.WriteLine(CalcBits(4));
            Console.WriteLine(CalcBits(68));
            Console.WriteLine(CalcBits(29));
            Console.WriteLine(CalcBits(12));
            Console.WriteLine(CalcBits(10));
            Console.WriteLine(CalcBits(30));
            Console.WriteLine("------------");

            // Enter  an array with unique numbers
            uint[] input = { 2, 10, 59, 29, 30, 12 };  // exp {4,12,10,30,58}
            uint[] test = GetSortedArrayByBits(input);

            foreach (uint val in test)
            {
                Console.WriteLine(val);
            }
        }
    }
}